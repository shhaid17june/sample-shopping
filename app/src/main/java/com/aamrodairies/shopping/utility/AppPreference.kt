package com.aamrodairies.shopping.utility

import android.content.Context
import com.aamrodairies.shopping.base.BaseSharedPreference

object AppPreference : BaseSharedPreference() {

    fun isProductAdded(mContext: Context?): Boolean {
        return getBoolean(mContext!!, Constants.Preference.IS_PRODUCT_ADDED)
    }

    fun isProductAdded(mContext: Context?, value: Boolean) {
        setBoolean(mContext!!, Constants.Preference.IS_PRODUCT_ADDED, value)
    }
}