package com.aamrodairies.shopping.base

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.recyclerview.widget.RecyclerView
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import kotlin.collections.ArrayList

abstract class BaseAdapter<T> : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var items: MutableList<T> = ArrayList()
    private var compositeDisposable = CompositeDisposable()
    protected var recyclerClickListener: MyRecyclerClickListener?= null

    protected fun addDisposable(disposable: Disposable){
        compositeDisposable.add(disposable)
    }

    fun setMyRecyclerClickListener(recyclerClickListener: MyRecyclerClickListener){
        this.recyclerClickListener = recyclerClickListener
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        onBindViewHold(holder, position)
    }

    override fun getItemCount(): Int = items.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(setItemLayout(), parent, false)
        return onCreateViewHold(view)
    }

    fun addItem(item : T){
        items.add(item)

        if(itemCount>0)
            notifyItemInserted(itemCount-1)
    }

    fun addItems(item : MutableList<T>){
        items.addAll(item)
        notifyDataSetChanged()
    }

    fun getItem(position: Int) : T = items[position]

    fun getAllItems():MutableList<T>{
        return items
    }

    fun removeItem(position: Int){
        items.removeAt(position)
        notifyItemRemoved(position)
    }

    fun removeAll(){
        items.clear()
        notifyDataSetChanged()
    }



    override fun onDetachedFromRecyclerView(recyclerView: RecyclerView) {
        compositeDisposable.clear()
        super.onDetachedFromRecyclerView(recyclerView)
    }

    @LayoutRes
    abstract fun setItemLayout(): Int
    abstract fun onBindViewHold(holder: RecyclerView.ViewHolder?, position: Int)
    abstract fun onCreateViewHold(view : View): RecyclerView.ViewHolder


}
