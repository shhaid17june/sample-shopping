package com.aamrodairies.shopping.utility

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentTransaction
import com.aamrodairies.shopping.R
import java.lang.Exception

object MyUtility {

    fun navigate(activity: FragmentActivity?, mFragment: Fragment, isBackStack: Boolean) {

        try {
            val mFragmentTransaction = activity?.supportFragmentManager?.beginTransaction()
            mFragmentTransaction?.apply {
                 setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                // setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
                replace(R.id.container, mFragment)

                if (isBackStack) {
                    addToBackStack(mFragment::javaClass.name)
                }
                commitAllowingStateLoss()
            }
        }catch (exception: Exception){
            exception.printStackTrace()
        }

    }
}