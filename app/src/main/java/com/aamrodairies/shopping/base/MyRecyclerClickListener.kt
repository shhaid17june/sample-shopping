package com.aamrodairies.shopping.base


interface MyRecyclerClickListener {
    fun onItemClicked(position : Int)
}