package com.aamrodairies.shopping.utility

object Constants{
    const val EMPTY = ""
    const val PAGE_TITLE = "PAGE_TITLE"

    object Preference{
        const val IS_PRODUCT_ADDED = "IS_PRODUCT_ADDED"
    }

}