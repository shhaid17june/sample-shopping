package com.aamrodairies.shopping.di.module

import com.aamrodairies.shopping.base.LoggingTree
import com.aamrodairies.shopping.base.NotLoggingTree
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import io.realm.Realm
import io.realm.RealmConfiguration
import javax.inject.Singleton


@Module
@InstallIn(ApplicationComponent::class)
object ApplicationModule {

    @Singleton
    @Provides
    fun getRealmConfiguration(): RealmConfiguration =
        RealmConfiguration.Builder()
            .name("simple.shopping")
            .schemaVersion(1)
            .build()

    @Singleton
    @Provides
    fun getRealm(realmConfiguration: RealmConfiguration): Realm =
        Realm.getInstance(realmConfiguration)


    @Singleton
    @Provides
    fun getLoggingTree(): LoggingTree =
        LoggingTree()

    @Singleton
    @Provides
    fun getNotLoggingTree(): NotLoggingTree =
        NotLoggingTree()


}