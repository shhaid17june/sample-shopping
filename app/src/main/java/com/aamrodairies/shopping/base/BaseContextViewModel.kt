package com.aamrodairies.shopping.base

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.ViewModel
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

abstract class BaseContextViewModel(mContext: Application) : AndroidViewModel(mContext) {

    protected val compositeDisposable: CompositeDisposable by lazy { CompositeDisposable() }


    protected fun addDisposable(disposable: Disposable){
        compositeDisposable.add(disposable)
    }

    override fun onCleared() {
        compositeDisposable.clear()
        super.onCleared()

    }

}