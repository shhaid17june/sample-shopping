package com.aamrodairies.shopping.base

import timber.log.Timber


class NotLoggingTree : Timber.Tree() {
    /*No need to print Log in release mode*/
    override fun log(priority: Int, tag: String?, message: String, t: Throwable?) {
        return
    }
}