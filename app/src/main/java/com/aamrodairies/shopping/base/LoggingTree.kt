package com.aamrodairies.shopping.base

import timber.log.Timber


class LoggingTree : Timber.DebugTree() {

    /*return log information in more detail when its in debug mode*/
    override fun createStackElementTag(element: StackTraceElement): String? {
        return String.format("C:%s:%s",
                super.createStackElementTag(element),
                element.lineNumber)
    }
}