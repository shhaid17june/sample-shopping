package com.aamrodairies.shopping.application

import android.app.Application
import com.aamrodairies.shopping.BuildConfig
import com.aamrodairies.shopping.base.LoggingTree
import com.aamrodairies.shopping.base.NotLoggingTree
import dagger.hilt.android.HiltAndroidApp
import timber.log.Timber
import javax.inject.Inject

@HiltAndroidApp
class MyApplication : Application() {

    @Inject lateinit var loggingTree: LoggingTree
    @Inject lateinit var notLoggingTree: NotLoggingTree

    override fun onCreate() {
        super.onCreate()
        if (BuildConfig.DEBUG){
            Timber.plant(loggingTree)
        }else{
            Timber.plant(notLoggingTree)
        }
    }
}