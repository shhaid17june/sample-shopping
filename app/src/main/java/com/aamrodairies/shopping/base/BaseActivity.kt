package com.aamrodairies.shopping.base

import android.os.Bundle
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.toolbar.*

abstract class BaseActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(setLayout())
        onViewCreated()
        setSupportActionBar(toolBar)
        toolBar?.background?.alpha = 0
        supportActionBar?.setDisplayShowTitleEnabled(false)

    }


    /*protected fun showToast(stringId: Int) {
        Toast.makeText(this, getString(stringId), Toast.LENGTH_LONG).show()
    }

    protected fun showToast(string: String) {
        Toast.makeText(this, string, Toast.LENGTH_SHORT).show()
    }*/


    @LayoutRes
    protected abstract fun setLayout(): Int

    protected abstract fun onViewCreated()

}