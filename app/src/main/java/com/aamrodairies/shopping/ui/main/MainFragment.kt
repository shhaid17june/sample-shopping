package com.aamrodairies.shopping.ui.main

import androidx.fragment.app.viewModels
import com.aamrodairies.shopping.R
import com.aamrodairies.shopping.base.BaseFragment
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainFragment : BaseFragment() {

    companion object {
        fun newInstance() = MainFragment()
    }

    private val mainViewModel: MainViewModel by viewModels()

    override fun setLayout(): Int = R.layout.main_fragment

    override fun onViewCreated() {

    }


}