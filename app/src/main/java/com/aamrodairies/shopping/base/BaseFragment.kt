package com.aamrodairies.shopping.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.LayoutRes
import androidx.appcompat.widget.AppCompatTextView
import androidx.fragment.app.Fragment
import com.aamrodairies.shopping.utility.Constants
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.toolbar.*
import timber.log.Timber

abstract class BaseFragment : Fragment() {

    protected val compositeDisposable: CompositeDisposable by lazy { CompositeDisposable() }
    private var myView: View? = null
    private var isFragmentExists: Boolean = false
    protected var tootBarTitle: AppCompatTextView? = null
    protected var text: String? = Constants.EMPTY

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        tootBarTitle = activity?.toolBarTitle
        isFragmentExists = true

        val bundle = arguments
        text = bundle?.getString(Constants.PAGE_TITLE)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (myView == null) {
            myView = inflater.inflate(setLayout(), container, false)
        }
        return myView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (isFragmentExists) {
            isFragmentExists = false
            onViewCreated()
        }
    }

    override fun onResume() {
        super.onResume()
        Timber.d("===========onResume=======text=====$text")
        setFragmentTitle(tootBarTitle, text)
    }

    override fun onPause() {
        super.onPause()
        Timber.d("===========onPause=======text=====$text")
    }


    protected fun showToast(stringId: Int) {
        Toast.makeText(activity, getString(stringId), Toast.LENGTH_LONG).show()
    }

    protected fun showToast(string: String) {
        Toast.makeText(activity, string, Toast.LENGTH_LONG).show()
    }

   /* protected fun showErrorMessageDialog(
        title: String,
        errorMessage: String,
        isWithRetry: Boolean
    ) {

        if (isWithRetry) {
            val mConfirmationDialog = CustomDialog.newInstance(
                title,
                errorMessage,
                getString(R.string.retry),
                getString(R.string.cancel)
            )

            mConfirmationDialog.setOnDialogButtonClickedListener(object :
                CustomDialog.OnDialogButtonClickedListener {
                override fun onPositiveButtonClicked() {

                }

                override fun onNegativeButtonClicked() {

                }

            })

            activity?.supportFragmentManager?.let {
                mConfirmationDialog.show(
                    it,
                    mConfirmationDialog::class.java.name
                )
            }
        }


    }
*/

    override fun onDestroy() {
        super.onDestroy()
        compositeDisposable.clear()
    }


    @LayoutRes
    protected abstract fun setLayout(): Int

    protected abstract fun onViewCreated()

    protected open fun setFragmentTitle(actionBarTitle: TextView?, title: String?){
        actionBarTitle?.text = title
    }



}