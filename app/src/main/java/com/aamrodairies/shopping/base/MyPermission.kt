package com.aamrodairies.shopping.base

import android.Manifest
import androidx.fragment.app.FragmentActivity
import com.tbruyelle.rxpermissions2.RxPermissions
import io.reactivex.disposables.Disposable

abstract class MyPermission(activity: FragmentActivity) {

    private val rxPermissions: RxPermissions by lazy { RxPermissions(activity) }
    val permissionLocation = arrayOf(Manifest.permission.ACCESS_FINE_LOCATION)
    val permissionCamera = arrayOf(Manifest.permission.CAMERA)
    val permissionAudio = arrayOf(Manifest.permission.RECORD_AUDIO)
    val permissionPhoneState = arrayOf(Manifest.permission.READ_PHONE_STATE)

    var permissionStorage = arrayOf(
        Manifest.permission.READ_EXTERNAL_STORAGE,
        Manifest.permission.WRITE_EXTERNAL_STORAGE
    )

    abstract fun permissionGranted(whichPermission: Array<String>)
    abstract fun permissionDecline(whichPermission: Array<String>)

    fun locationPermission(): Disposable? {
        return rxPermissions
            .request(permissionLocation[0])
            .subscribe { granted ->
                if (granted) {
                    permissionGranted(whichPermission = permissionLocation)
                } else {
                    permissionDecline(whichPermission = permissionLocation)
                }

            }
    }

    /**
     * Check Storage permission for Read and write file on device.
     */

    fun checkStoragePermissions(): Disposable? {
        return rxPermissions
            .request(permissionStorage[0], permissionStorage[1])
            .subscribe { granted ->
                if (granted) {
                    permissionGranted(permissionStorage)
                } else {
                    permissionDecline(permissionStorage)
                }
            }
    }


    fun cameraPermission(): Disposable? {
        return rxPermissions
            .request(permissionCamera[0])
            .subscribe { granted ->
                if (granted) {
                    permissionGranted(whichPermission = permissionCamera)
                } else {
                    permissionDecline(whichPermission = permissionCamera)
                }

            }
    }


    fun audioPermission(): Disposable? {
        return rxPermissions
            .request(permissionAudio[0])
            .subscribe { granted ->
                if (granted) {
                    permissionGranted(whichPermission = permissionAudio)
                } else {
                    permissionDecline(whichPermission = permissionAudio)
                }

            }
    }

    fun phoneStatePermission(): Disposable? {
        return rxPermissions
            .request(permissionPhoneState[0])
            .subscribe { granted ->
                if (granted) {
                    permissionGranted(whichPermission = permissionPhoneState)
                } else {
                    permissionDecline(whichPermission = permissionPhoneState)
                }

            }
    }


}