package com.aamrodairies.shopping.base

import android.view.View

interface MyRecyclerLongClickListener {
    fun onItemLongClicked(position: Int, view: View)
}