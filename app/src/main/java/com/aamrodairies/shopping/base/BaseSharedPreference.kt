package com.aamrodairies.shopping.base

import android.content.Context
import android.content.SharedPreferences
import androidx.preference.PreferenceManager
import com.aamrodairies.shopping.utility.Constants


abstract class BaseSharedPreference {

    private fun getSharedPreferences(ctx: Context): SharedPreferences {
        return PreferenceManager.getDefaultSharedPreferences(ctx)
    }

    protected fun setString(mContext: Context, key: String, value: String?){
        getSharedPreferences(mContext).edit().run {
            putString(key, value)
            apply()
        }
    }

    protected fun getString(mContext: Context, key: String):String?{
        return getSharedPreferences(mContext).run {getString(key, Constants.EMPTY)}
    }



    protected fun setBoolean(mContext: Context, key: String, value: Boolean){
        getSharedPreferences(mContext).edit().run {
            putBoolean(key, value)
            apply()
        }
    }

    protected fun getBoolean(mContext: Context, key: String):Boolean {
        return getSharedPreferences(mContext).run {getBoolean(key, false)}
    }



    protected fun setInt(mContext: Context, key: String, value: Int){
        getSharedPreferences(mContext).edit().run {
            putInt(key, value)
            apply()
        }
    }

    protected fun getInt(mContext: Context, key: String):Int{
        return getSharedPreferences(mContext).run {getInt(key, -1)}
    }



    protected fun setFloat(mContext: Context, key: String, value: Float){
        getSharedPreferences(mContext).edit().run {
            putFloat(key, value)
            apply()
        }
    }

    protected fun getFloat(mContext: Context, key: String):Float{
        return getSharedPreferences(mContext).run {getFloat(key, -1f)}
    }

}